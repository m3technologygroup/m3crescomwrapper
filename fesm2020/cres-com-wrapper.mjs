import * as i0 from '@angular/core';
import { Injectable, Directive, Input, HostListener, NgModule } from '@angular/core';
import { Observable } from 'rxjs';

class DigitalSigObserver {
    constructor() { }
    static Observe(signal) {
        return new Observable((observer) => {
            let CresSubHandel = CrComLib.subscribeState('b', signal, (val) => {
                observer.next(val);
            });
            return () => {
                CrComLib.unsubscribeState('b', signal, CresSubHandel);
            };
        });
    }
}
DigitalSigObserver.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DigitalSigObserver, deps: [], target: i0.ɵɵFactoryTarget.Injectable });
DigitalSigObserver.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DigitalSigObserver, providedIn: 'root' });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DigitalSigObserver, decorators: [{
            type: Injectable,
            args: [{
                    providedIn: 'root'
                }]
        }], ctorParameters: function () { return []; } });

class AnalogSigObserver {
    constructor() { }
    static Observe(signal) {
        return new Observable((observer) => {
            let CresSubHandel = CrComLib.subscribeState('n', signal, (val) => {
                observer.next(val);
            });
            return () => {
                CrComLib.unsubscribeState('n', signal, CresSubHandel);
            };
        });
    }
}
AnalogSigObserver.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: AnalogSigObserver, deps: [], target: i0.ɵɵFactoryTarget.Injectable });
AnalogSigObserver.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: AnalogSigObserver, providedIn: 'root' });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: AnalogSigObserver, decorators: [{
            type: Injectable,
            args: [{
                    providedIn: 'root'
                }]
        }], ctorParameters: function () { return []; } });

class SerialSigObserver {
    constructor() { }
    static Observe(signal) {
        return new Observable((observer) => {
            let CresSubHandel = CrComLib.subscribeState('s', signal, (val) => {
                observer.next(val);
            });
            return () => {
                CrComLib.unsubscribeState('s', signal, CresSubHandel);
            };
        });
    }
}
SerialSigObserver.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: SerialSigObserver, deps: [], target: i0.ɵɵFactoryTarget.Injectable });
SerialSigObserver.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: SerialSigObserver, providedIn: 'root' });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: SerialSigObserver, decorators: [{
            type: Injectable,
            args: [{
                    providedIn: 'root'
                }]
        }], ctorParameters: function () { return []; } });

class CresDigitalSendDirective {
    constructor() {
        //handel for the interval used for press and hold
        this.interval = null;
        this.Signal = "";
        this.PressHold = "false";
    }
    //handel Toutch inputs
    onTouchStart(ev) {
        ev.preventDefault();
        if (this.PressHold === "true") {
            this.sendPress();
        }
        else {
            this.sendPulse();
        }
    }
    onTouchEnd(ev) {
        ev.preventDefault();
        if (this.PressHold === "true") {
            this.sendRelease();
        }
    }
    //Handel Mouse inputs for Xpanel
    onMouseDown() {
        if (this.PressHold === "true") {
            this.sendPress();
        }
        else {
            this.sendPulse();
        }
    }
    onMouseUp() {
        if (this.PressHold === "true") {
            this.sendRelease();
        }
    }
    //send an instantanious pulse to the CS
    sendPulse() {
        if (this.Signal.length > 0) {
            CrComLib.publishEvent('b', this.Signal, true);
            CrComLib.publishEvent('b', this.Signal, false);
        }
    }
    //send repeat digital event for press and hold.
    //250ms repeat time required by crestron
    sendPress() {
        CrComLib.publishEvent('o', this.Signal, { repeatdigital: true });
        this.interval = setInterval(() => {
            CrComLib.publishEvent('o', this.Signal, { repeatdigital: true });
        }, 250);
    }
    sendRelease() {
        if (this.interval !== null) {
            clearInterval(this.interval);
        }
        CrComLib.publishEvent('o', this.Signal, { repeatdigital: false });
    }
}
CresDigitalSendDirective.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: CresDigitalSendDirective, deps: [], target: i0.ɵɵFactoryTarget.Directive });
CresDigitalSendDirective.ɵdir = i0.ɵɵngDeclareDirective({ minVersion: "12.0.0", version: "13.0.2", type: CresDigitalSendDirective, selector: "[CresDigitalSend]", inputs: { Signal: "Signal", PressHold: "PressHold" }, host: { listeners: { "touchstart": "onTouchStart($event)", "touchend": "onTouchEnd($event)", "mousedown": "onMouseDown()", "mouseup": "onMouseUp()" } }, ngImport: i0 });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: CresDigitalSendDirective, decorators: [{
            type: Directive,
            args: [{
                    selector: '[CresDigitalSend]'
                }]
        }], ctorParameters: function () { return []; }, propDecorators: { Signal: [{
                type: Input
            }], PressHold: [{
                type: Input
            }], onTouchStart: [{
                type: HostListener,
                args: ['touchstart', ['$event']]
            }], onTouchEnd: [{
                type: HostListener,
                args: ['touchend', ['$event']]
            }], onMouseDown: [{
                type: HostListener,
                args: ['mousedown']
            }], onMouseUp: [{
                type: HostListener,
                args: ['mouseup']
            }] } });

class CresDigitalFeedbackDirective {
    constructor(renderer, elementRef, changeDetection) {
        this.renderer = renderer;
        this.elementRef = elementRef;
        this.changeDetection = changeDetection;
        this.FbSignal = "";
        this.FbClass = "";
        //stores the subscription ID for the FB state
        this.CresSubHandel = "";
    }
    ngOnInit() {
        this.changeDetection.detach();
    }
    //If a Feedback signal is specified, subscribe to it
    ngAfterViewInit() {
        if (this.FbSignal.length > 0) {
            this.CresSubHandel = CrComLib.subscribeState('b', this.FbSignal, (state) => {
                if (state) {
                    this.applyFbActiveClass();
                }
                else {
                    this.removeFbActiveClass();
                }
                this.changeDetection.detectChanges();
            });
        }
    }
    //Clean up FB sub if it exists, prevent memory leekage
    ngOnDestroy() {
        if (this.CresSubHandel.length > 0)
            CrComLib.unsubscribeState('b', this.FbSignal, this.CresSubHandel);
    }
    //If a class is specified, add it to the applied element
    applyFbActiveClass() {
        if (this.FbClass.length > 0) {
            switch (this.FbClass) {
                case "primary": //using global primary
                    this.renderer.addClass(this.elementRef.nativeElement, "m3-global-primary-bg");
                    break;
                case "accent": //using accent primary
                    this.renderer.addClass(this.elementRef.nativeElement, "m3-global-accent-bg");
                    break;
                case "warn": //using warn primary
                    this.renderer.addClass(this.elementRef.nativeElement, "m3-global-warn-bg");
                    break;
                default: //using custom class
                    this.renderer.addClass(this.elementRef.nativeElement, this.FbClass);
                    break;
            }
        }
    }
    //If a class is specified remove it from the applied element
    removeFbActiveClass() {
        if (this.FbClass.length > 0) {
            switch (this.FbClass) {
                case "primary": //using global primary
                    this.renderer.removeClass(this.elementRef.nativeElement, "m3-global-primary-bg");
                    break;
                case "accent": //using accent primary
                    this.renderer.removeClass(this.elementRef.nativeElement, "m3-global-accent-bg");
                    break;
                case "warn": //using warn primary
                    this.renderer.removeClass(this.elementRef.nativeElement, "m3-global-warn-bg");
                    break;
                default: //using custom class
                    this.renderer.removeClass(this.elementRef.nativeElement, this.FbClass);
                    break;
            }
        }
    }
}
CresDigitalFeedbackDirective.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: CresDigitalFeedbackDirective, deps: [{ token: i0.Renderer2 }, { token: i0.ElementRef }, { token: i0.ChangeDetectorRef }], target: i0.ɵɵFactoryTarget.Directive });
CresDigitalFeedbackDirective.ɵdir = i0.ɵɵngDeclareDirective({ minVersion: "12.0.0", version: "13.0.2", type: CresDigitalFeedbackDirective, selector: "[CresDigitalFeedback]", inputs: { FbSignal: "FbSignal", FbClass: "FbClass" }, ngImport: i0 });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: CresDigitalFeedbackDirective, decorators: [{
            type: Directive,
            args: [{
                    selector: '[CresDigitalFeedback]'
                }]
        }], ctorParameters: function () { return [{ type: i0.Renderer2 }, { type: i0.ElementRef }, { type: i0.ChangeDetectorRef }]; }, propDecorators: { FbSignal: [{
                type: Input
            }], FbClass: [{
                type: Input
            }] } });

class CresComWrapperModule {
}
CresComWrapperModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: CresComWrapperModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
CresComWrapperModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: CresComWrapperModule, declarations: [CresDigitalSendDirective,
        CresDigitalFeedbackDirective], exports: [CresDigitalSendDirective,
        CresDigitalFeedbackDirective] });
CresComWrapperModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: CresComWrapperModule, imports: [[]] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: CresComWrapperModule, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [
                        CresDigitalSendDirective,
                        CresDigitalFeedbackDirective
                    ],
                    imports: [],
                    exports: [
                        CresDigitalSendDirective,
                        CresDigitalFeedbackDirective
                    ]
                }]
        }] });

class EmulatorLoader {
    constructor() {
        this.ch5Emulator = CrComLib.Ch5Emulator.getInstance();
    }
    // init emulator
    initEmulator(emulator) {
        CrComLib.Ch5Emulator.clear();
        this.ch5Emulator = CrComLib.Ch5Emulator.getInstance();
        this.ch5Emulator.loadScenario(emulator);
        this.ch5Emulator.run();
    }
}
EmulatorLoader.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: EmulatorLoader, deps: [], target: i0.ɵɵFactoryTarget.Injectable });
EmulatorLoader.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: EmulatorLoader, providedIn: 'root' });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: EmulatorLoader, decorators: [{
            type: Injectable,
            args: [{
                    providedIn: 'root'
                }]
        }], ctorParameters: function () { return []; } });

class SendDigital {
    constructor() { }
    static Pulse(signal) {
        CrComLib.publishEvent('b', signal, true);
        CrComLib.publishEvent('b', signal, false);
    }
    static Value(signal, state) {
        CrComLib.publishEvent('b', signal, state);
    }
}
SendDigital.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: SendDigital, deps: [], target: i0.ɵɵFactoryTarget.Injectable });
SendDigital.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: SendDigital, providedIn: 'root' });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: SendDigital, decorators: [{
            type: Injectable,
            args: [{
                    providedIn: 'root'
                }]
        }], ctorParameters: function () { return []; } });

class SendAnalog {
    constructor() { }
    static Value(signal, value) {
        CrComLib.publishEvent('n', signal, value);
    }
}
SendAnalog.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: SendAnalog, deps: [], target: i0.ɵɵFactoryTarget.Injectable });
SendAnalog.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: SendAnalog, providedIn: 'root' });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: SendAnalog, decorators: [{
            type: Injectable,
            args: [{
                    providedIn: 'root'
                }]
        }], ctorParameters: function () { return []; } });

class SendString {
    constructor() { }
    static Value(signal, value) {
        CrComLib.publishEvent('s', signal, value);
    }
}
SendString.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: SendString, deps: [], target: i0.ɵɵFactoryTarget.Injectable });
SendString.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: SendString, providedIn: 'root' });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: SendString, decorators: [{
            type: Injectable,
            args: [{
                    providedIn: 'root'
                }]
        }], ctorParameters: function () { return []; } });

/*
 * Public API Surface of cres-com-wrapper
 */

/**
 * Generated bundle index. Do not edit.
 */

export { AnalogSigObserver, CresComWrapperModule, CresDigitalFeedbackDirective, CresDigitalSendDirective, DigitalSigObserver, EmulatorLoader, SendAnalog, SendDigital, SendString, SerialSigObserver };
//# sourceMappingURL=cres-com-wrapper.mjs.map
