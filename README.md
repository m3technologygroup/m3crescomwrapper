# CresComWrapper

The is an M3 Technology Group Wrapper for the Crestron CH5 CrComLib.

It exists to provide standard angular hooks for crestron comunication wihtout using the CH5 Components.

# Version Histroy
- 0.2.1 - Added ability to use global theme in CresDigitalFb
- 0.2.0 - Upgraded @Angular/core for Angular 13.x support
- 0.1.0 - initial release, Angular 12.x support

# Installation
Install the Crestron CH5-CrComLib First!
`npm install @creston/ch5-crcomlib`

Then from an angular project, fun the following:
>`npm install https://bitbucket.org/m3technologygroup/m3crescomwrapper.git`

**Then read the next section as you will likley get an error on compile...**

# If you get an error on compile
At the time of writing, you will need to add the CrComLib to the script import array of angular.json.

## Fix
To fix this (after installing @crestron/ch5-crcomlib from npm), open 
`angular.json`
And add the following scripts array for both build and test enviroments:
`"./node_modules/@crestron/ch5-crcomlib/build_bundles/umd/cr-com-lib.js"`


# Usage
The cres-com-wrapper provides several ways to interact with the control system.

## Button Directives
The easiest way to tie a button to a join or signal name is to use the included directives.

### Setup.
To enable the wrappers button directives in your project, you will need to import the `CresComWrapperModule` in your `app.module.ts` file. As such:
>` import { CresComWrapperModule } from "cres-com-wrapper";`
>`imports: [ …, CresComWrapperModule ]`

### Sending a Digital pulse from a button using directives
Simply add the `CresDigitalSend` directive to any HTML object in the template. Use the `Signal` attribute to specify either an int join number or a Signal name.

Here is an example button that sends a single pulse to join 20:
>` <button CresDigitalSend Signal="20"></button>`

### Sending a Digital rising and falling edges using directrives
The `PressHold` attribute takes a boolean values that allow the button to act more like a traditional Creston button (rising edge on press and falling edge on release). Set this to `true` to enable this functionality.

**This is not recommended unless you specifically need to know the duration of a press. (EG Volume controls or camera controls)**

This is an example of a button that sends a rising edge on press and a falling edge on release to the signal named “Audio.VolUp”
>`<button CresDigitalSend Signal="Audio.VolUp" PressHold="true"></button>`

**NOTE: The CH5 Emulator cannot respond to buttons where `PressHold=”true”` is specified, but control systems will respond as expected.**

### Providing Feedback to buttons using directives
Control system FB is easy to implement with the `CresDigitalFeedback` directive.

The `FbSignal` attribute represents the join number or signal name to be used for the feedback state.
`FbClass` is a required parameter that specifies a CSS class to use for the active state of the button.

`FbClass` can be given a value of `primary`, `accent` or `warn` to use a class that sets the background-color to the global theme colors (if M3Template is installed)

The following is an example of a button that sends a rising edge to `Input.1.Select` and receives feedback from `Input.1.Acvtive` and applies the CSS class `BtnActive` when `Input.1.Active` is high.
>`<button CresDigitalSend Signal="Input.1.Select" CresDigitalFeedback  FbSignal="Input.1.Active" FbClass="BtnActive"> </button>`


# Usage
The cres-com-wrapper provides several ways to interact with the control system.

## Button Directives
The easiest way to tie a button to a join or signal name is to use the included directives.

### Setup.
To enable the wrappers button directives in your project, you will need to import the `CresComWrapperModule` in your `app.module.ts` file. As such:
>` import { CresComWrapperModule } from "cres-com-wrapper";`
>`imports: [ …, CresComWrapperModule ]`

### Sending a Digital pulse from a button using directives
Simply add the `CresDigitalSend` directive to any HTML object in the template. Use the `Signal` attribute to specify either an int join number or a Signal name.

Here is an example button that sends a single pulse to join 20:
>` <button CresDigitalSend Signal="20"></button>`

### Sending a Digital rising and falling edges using directrives
The `PressHold` attribute takes a boolean values that allow the button to act more like a traditional Creston button (rising edge on press and falling edge on release). Set this to `true` to enable this functionality.

**This is not recommended unless you specifically need to know the duration of a press. (EG Volume controls or camera controls)**

This is an example of a button that sends a rising edge on press and a falling edge on release to the signal named “Audio.VolUp”
>`<button CresDigitalSend Signal="Audio.VolUp" PressHold="true"></button>`

**NOTE: The CH5 Emulator cannot respond to buttons where `PressHold=”true”` is specified, but control systems will respond as expected.**

### Providing Feedback to buttons using directives
Control system FB is easy to implement with the `CresDigitalFeedback` directive.

The `FbSignal` attribute represents the join number or signal name to be used for the feedback state.
`FbClass` is a required parameter that specifies a CSS class to use for the active state of the button.

The following is an example of a button that sends a rising edge to `Input.1.Select` and receives feedback from `Input.1.Acvtive` and applies the CSS class `BtnActive` when `Input.1.Active` is high.
>`<button CresDigitalSend Signal="Input.1.Select" CresDigitalFeedback  FbSignal="Input.1.Active" FbClass="BtnActive"> </button>`


## Getting Feedback from the control system
Cres-com-wrapper provides methods to subscribe to control system feedback using RxJs Observables.

### Signal Observers
There are 3 different helper services: `DigitalSigObserver`, `AnalogSigObserver`, and `SerialSigObserver` which handle Digital, Analog, and Serial subscriptions.

Each of these classes has an `Observe` method that takes a single argument of a signal name or Join number, and then return an RxJs Observable of the requested signal type.

### Using a Signal Observer
The process for subscribing to a signal is the same regardless of signal type (Digital, Analog, Serial).

The following is an example of a digital signal named `MyDigitalSig`. The observable is stored in a variable named `Digital`.

In the component class:
>`Digital?:Observable<Boolean>;`

In ngOnInit:
>`this.Digital = DigitalSigObserver.Observe("MyDigitalSig ");`

The `Digital` var now contains a cold observable linked to the digital signal `MyDigitalSig`
### Observable in the HTML template
Once you have an observable, it is easy to use in the HTML Template using Angular’s async pipe. The following hides an HTML element when the `Digital` observable from above is `false` and shows the element when the state is `true`.
>`<div *ngIf=”Digital | async”>…….</div>`

Analog and serial observables can also be used this way:
>`<div *ngFor=”let idx of Analog | async”> …. </div>`
>`<p>My String says: {{ Serial | async }}</p>`

The async pipe will automatically handle subscribing and unsubscribing from the observable without additional code.
The mechanics of Angular directives and the async pipe are beyond the scope of this help document.

### Subscribing to an observable in Typescript
If a signal is needed in the component Typescript code, it must be subscribed to with the `subscribe` method.
**Always unsubscribe from an observable when a component is destroyed or the signal is no longer needed.**
How to use RxJs observables is outside the scope of this document, but here is an example that subscribes to a signal named `mySerial` and logs it to the console:

Component class:
>` serialSubscription?:Subscription;`

ngOnInit:
>` this.serialSubscription = SerialSigObserver.Observe("mySerial")`
`    .subscribe( (val) => {console.log(val);`
`});`

ngOnDestroy:
>`this.serialSubscription?.unsubscribe()`

## Sending Signals to the Control System
While it is recommended to use the provided directives for any button based actions, it may be neccessary to send data from TypeScript Code.

There are 3 classes provided for sending digital, analog and serial signals. names `sendDigital`, `sendAnalog`, and `sendSerial` respectivly.

All of these conain a method called `Value`, that takes an argument of a signal name followed by the value.

`sendDigital` also has a method called `Pulse` that takes an argument of a signal name. This sends a rising edge to trigger an action.

### Exmaples
`sendDigital.Value("MyDigital",true)`   Sends the value of true to the signal `MyDigital`
`sendDigital.Pulse("MyDigital")`   Sends a pulse (rising and falling edge) to `MyDigital`
`sendString.Value("MySerial","Hello World!")`   Sends the string of "Hello World!" to the signal `MySerial`


## Loading the CH5 Emulator
The CresComWrapper also has a basic class that provides the boilerplate code necessary to start the CH5 emulator.

### Injecting the Emulator Loader
The Emulator Loader is an angular service that handles the loading of the CH5 Emulator.

We recommend that the emulator be loaded in AppModule.ts

Here is an example of how that is done:

Import the Emulator data as JSON, as well as the emulator loader:
>`import * as EmulatorData from '../assets/data/emulator.json'`
>`import { EmulatorLoader } from 'cres-com-wrapper'`

Inject the Emulator loader in the constructor and load the emulator data:
>`constructor( private emu:EmulatorLoader){emu.initEmulator(EmulatorData);}`

### Note about the JSON import warning
If you get a compiler warning that says `Cannot find module …. Consider using '--resolveJsonModule'`, then you will have to add the following to your `tsconfig.json` file:
>`"resolveJsonModule": true`