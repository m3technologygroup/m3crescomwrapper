import * as i0 from "@angular/core";
export declare class CresDigitalSendDirective {
    constructor();
    interval: any;
    Signal: string;
    PressHold: "true" | "false";
    onTouchStart(ev: any): void;
    onTouchEnd(ev: any): void;
    onMouseDown(): void;
    onMouseUp(): void;
    sendPulse(): void;
    sendPress(): void;
    sendRelease(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<CresDigitalSendDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<CresDigitalSendDirective, "[CresDigitalSend]", never, { "Signal": "Signal"; "PressHold": "PressHold"; }, {}, never>;
}
