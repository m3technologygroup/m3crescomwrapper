import * as i0 from "@angular/core";
export declare class SendAnalog {
    constructor();
    static Value(signal: string, value: number): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<SendAnalog, never>;
    static ɵprov: i0.ɵɵInjectableDeclaration<SendAnalog>;
}
