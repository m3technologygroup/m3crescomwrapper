import { Observable } from 'rxjs';
import * as i0 from "@angular/core";
export declare class SerialSigObserver {
    constructor();
    static Observe(signal: string): Observable<string>;
    static ɵfac: i0.ɵɵFactoryDeclaration<SerialSigObserver, never>;
    static ɵprov: i0.ɵɵInjectableDeclaration<SerialSigObserver>;
}
