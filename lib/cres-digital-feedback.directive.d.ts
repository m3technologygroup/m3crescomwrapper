import { AfterViewInit, ElementRef, Renderer2, OnDestroy, ChangeDetectorRef, OnInit } from '@angular/core';
import * as i0 from "@angular/core";
export declare class CresDigitalFeedbackDirective implements OnInit, AfterViewInit, OnDestroy {
    private renderer;
    private elementRef;
    private changeDetection;
    FbSignal: string;
    FbClass: string;
    CresSubHandel: string;
    constructor(renderer: Renderer2, elementRef: ElementRef, changeDetection: ChangeDetectorRef);
    ngOnInit(): void;
    ngAfterViewInit(): void;
    ngOnDestroy(): void;
    applyFbActiveClass(): void;
    removeFbActiveClass(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<CresDigitalFeedbackDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<CresDigitalFeedbackDirective, "[CresDigitalFeedback]", never, { "FbSignal": "FbSignal"; "FbClass": "FbClass"; }, {}, never>;
}
