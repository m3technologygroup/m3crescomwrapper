import { Observable } from 'rxjs';
import * as i0 from "@angular/core";
export declare class DigitalSigObserver {
    constructor();
    static Observe(signal: string): Observable<boolean>;
    static ɵfac: i0.ɵɵFactoryDeclaration<DigitalSigObserver, never>;
    static ɵprov: i0.ɵɵInjectableDeclaration<DigitalSigObserver>;
}
