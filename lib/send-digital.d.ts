import * as i0 from "@angular/core";
export declare class SendDigital {
    constructor();
    static Pulse(signal: string): void;
    static Value(signal: string, state: boolean): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<SendDigital, never>;
    static ɵprov: i0.ɵɵInjectableDeclaration<SendDigital>;
}
