import { Ch5Emulator } from "@crestron/ch5-crcomlib/build_bundles/umd/@types";
import * as i0 from "@angular/core";
export declare class EmulatorLoader {
    ch5Emulator: Ch5Emulator;
    constructor();
    initEmulator(emulator: any): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<EmulatorLoader, never>;
    static ɵprov: i0.ɵɵInjectableDeclaration<EmulatorLoader>;
}
