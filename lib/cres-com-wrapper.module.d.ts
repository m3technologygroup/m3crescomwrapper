import * as i0 from "@angular/core";
import * as i1 from "./cres-digital-send.directive";
import * as i2 from "./cres-digital-feedback.directive";
export declare class CresComWrapperModule {
    static ɵfac: i0.ɵɵFactoryDeclaration<CresComWrapperModule, never>;
    static ɵmod: i0.ɵɵNgModuleDeclaration<CresComWrapperModule, [typeof i1.CresDigitalSendDirective, typeof i2.CresDigitalFeedbackDirective], never, [typeof i1.CresDigitalSendDirective, typeof i2.CresDigitalFeedbackDirective]>;
    static ɵinj: i0.ɵɵInjectorDeclaration<CresComWrapperModule>;
}
