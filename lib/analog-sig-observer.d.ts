import { Observable } from 'rxjs';
import * as i0 from "@angular/core";
export declare class AnalogSigObserver {
    constructor();
    static Observe(signal: string): Observable<number>;
    static ɵfac: i0.ɵɵFactoryDeclaration<AnalogSigObserver, never>;
    static ɵprov: i0.ɵɵInjectableDeclaration<AnalogSigObserver>;
}
