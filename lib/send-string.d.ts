import * as i0 from "@angular/core";
export declare class SendString {
    constructor();
    static Value(signal: string, value: string): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<SendString, never>;
    static ɵprov: i0.ɵɵInjectableDeclaration<SendString>;
}
