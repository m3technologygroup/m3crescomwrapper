import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
export class SendDigital {
    constructor() { }
    static Pulse(signal) {
        CrComLib.publishEvent('b', signal, true);
        CrComLib.publishEvent('b', signal, false);
    }
    static Value(signal, state) {
        CrComLib.publishEvent('b', signal, state);
    }
}
SendDigital.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: SendDigital, deps: [], target: i0.ɵɵFactoryTarget.Injectable });
SendDigital.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: SendDigital, providedIn: 'root' });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: SendDigital, decorators: [{
            type: Injectable,
            args: [{
                    providedIn: 'root'
                }]
        }], ctorParameters: function () { return []; } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VuZC1kaWdpdGFsLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vcHJvamVjdHMvY3Jlcy1jb20td3JhcHBlci9zcmMvbGliL3NlbmQtZGlnaXRhbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDOztBQU8zQyxNQUFNLE9BQU8sV0FBVztJQUV0QixnQkFBZ0IsQ0FBQztJQUVqQixNQUFNLENBQUMsS0FBSyxDQUFDLE1BQWE7UUFDeEIsUUFBUSxDQUFDLFlBQVksQ0FBQyxHQUFHLEVBQUMsTUFBTSxFQUFDLElBQUksQ0FBQyxDQUFBO1FBQ3RDLFFBQVEsQ0FBQyxZQUFZLENBQUMsR0FBRyxFQUFDLE1BQU0sRUFBQyxLQUFLLENBQUMsQ0FBQTtJQUN6QyxDQUFDO0lBRUQsTUFBTSxDQUFDLEtBQUssQ0FBQyxNQUFhLEVBQUMsS0FBYTtRQUN0QyxRQUFRLENBQUMsWUFBWSxDQUFDLEdBQUcsRUFBQyxNQUFNLEVBQUMsS0FBSyxDQUFDLENBQUE7SUFDekMsQ0FBQzs7d0dBWFUsV0FBVzs0R0FBWCxXQUFXLGNBRlYsTUFBTTsyRkFFUCxXQUFXO2tCQUh2QixVQUFVO21CQUFDO29CQUNWLFVBQVUsRUFBRSxNQUFNO2lCQUNuQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbmRlY2xhcmUgdmFyIENyQ29tTGliOiBhbnk7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBTZW5kRGlnaXRhbCB7XHJcblxyXG4gIGNvbnN0cnVjdG9yKCkgeyB9XHJcblxyXG4gIHN0YXRpYyBQdWxzZShzaWduYWw6c3RyaW5nKTp2b2lkIHtcclxuICAgIENyQ29tTGliLnB1Ymxpc2hFdmVudCgnYicsc2lnbmFsLHRydWUpXHJcbiAgICBDckNvbUxpYi5wdWJsaXNoRXZlbnQoJ2InLHNpZ25hbCxmYWxzZSlcclxuICB9XHJcblxyXG4gIHN0YXRpYyBWYWx1ZShzaWduYWw6c3RyaW5nLHN0YXRlOmJvb2xlYW4pOnZvaWQge1xyXG4gICAgQ3JDb21MaWIucHVibGlzaEV2ZW50KCdiJyxzaWduYWwsc3RhdGUpXHJcbiAgfVxyXG59XHJcbiJdfQ==