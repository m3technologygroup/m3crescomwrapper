import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import * as i0 from "@angular/core";
export class SerialSigObserver {
    constructor() { }
    static Observe(signal) {
        return new Observable((observer) => {
            let CresSubHandel = CrComLib.subscribeState('s', signal, (val) => {
                observer.next(val);
            });
            return () => {
                CrComLib.unsubscribeState('s', signal, CresSubHandel);
            };
        });
    }
}
SerialSigObserver.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: SerialSigObserver, deps: [], target: i0.ɵɵFactoryTarget.Injectable });
SerialSigObserver.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: SerialSigObserver, providedIn: 'root' });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: SerialSigObserver, decorators: [{
            type: Injectable,
            args: [{
                    providedIn: 'root'
                }]
        }], ctorParameters: function () { return []; } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VyaWFsLXNpZy1vYnNlcnZlci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3Byb2plY3RzL2NyZXMtY29tLXdyYXBwZXIvc3JjL2xpYi9zZXJpYWwtc2lnLW9ic2VydmVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLE1BQU0sQ0FBQzs7QUFPbEMsTUFBTSxPQUFPLGlCQUFpQjtJQUU1QixnQkFBZ0IsQ0FBQztJQUVqQixNQUFNLENBQUMsT0FBTyxDQUFDLE1BQWE7UUFDMUIsT0FBTyxJQUFJLFVBQVUsQ0FBQyxDQUFDLFFBQVEsRUFBRSxFQUFFO1lBQ2pDLElBQUksYUFBYSxHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsR0FBRyxFQUFDLE1BQU0sRUFBRSxDQUFDLEdBQVUsRUFBRSxFQUFFO2dCQUNyRSxRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ3JCLENBQUMsQ0FBQyxDQUFBO1lBRUYsT0FBTyxHQUFHLEVBQUU7Z0JBQ1YsUUFBUSxDQUFDLGdCQUFnQixDQUFDLEdBQUcsRUFBQyxNQUFNLEVBQUMsYUFBYSxDQUFDLENBQUE7WUFDckQsQ0FBQyxDQUFBO1FBQ0gsQ0FBQyxDQUFDLENBQUE7SUFDSixDQUFDOzs4R0FkVSxpQkFBaUI7a0hBQWpCLGlCQUFpQixjQUZoQixNQUFNOzJGQUVQLGlCQUFpQjtrQkFIN0IsVUFBVTttQkFBQztvQkFDVixVQUFVLEVBQUUsTUFBTTtpQkFDbkIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XG5cbmRlY2xhcmUgdmFyIENyQ29tTGliOiBhbnk7XG5cbkBJbmplY3RhYmxlKHtcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuZXhwb3J0IGNsYXNzIFNlcmlhbFNpZ09ic2VydmVyIHtcblxuICBjb25zdHJ1Y3RvcigpIHsgfVxuXG4gIHN0YXRpYyBPYnNlcnZlKHNpZ25hbDpzdHJpbmcpOk9ic2VydmFibGU8c3RyaW5nPiB7XG4gICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKChvYnNlcnZlcikgPT4ge1xuICAgICAgbGV0IENyZXNTdWJIYW5kZWwgPSBDckNvbUxpYi5zdWJzY3JpYmVTdGF0ZSgncycsc2lnbmFsLCAodmFsOnN0cmluZykgPT4ge1xuICAgICAgICBvYnNlcnZlci5uZXh0KHZhbCk7XG4gICAgICB9KVxuICAgICAgXG4gICAgICByZXR1cm4gKCkgPT4ge1xuICAgICAgICBDckNvbUxpYi51bnN1YnNjcmliZVN0YXRlKCdzJyxzaWduYWwsQ3Jlc1N1YkhhbmRlbClcbiAgICAgIH1cbiAgICB9KSBcbiAgfVxufVxuIl19