import { Directive, Input } from '@angular/core';
import * as i0 from "@angular/core";
export class CresDigitalFeedbackDirective {
    constructor(renderer, elementRef, changeDetection) {
        this.renderer = renderer;
        this.elementRef = elementRef;
        this.changeDetection = changeDetection;
        this.FbSignal = "";
        this.FbClass = "";
        //stores the subscription ID for the FB state
        this.CresSubHandel = "";
    }
    ngOnInit() {
        this.changeDetection.detach();
    }
    //If a Feedback signal is specified, subscribe to it
    ngAfterViewInit() {
        if (this.FbSignal.length > 0) {
            this.CresSubHandel = CrComLib.subscribeState('b', this.FbSignal, (state) => {
                if (state) {
                    this.applyFbActiveClass();
                }
                else {
                    this.removeFbActiveClass();
                }
                this.changeDetection.detectChanges();
            });
        }
    }
    //Clean up FB sub if it exists, prevent memory leekage
    ngOnDestroy() {
        if (this.CresSubHandel.length > 0)
            CrComLib.unsubscribeState('b', this.FbSignal, this.CresSubHandel);
    }
    //If a class is specified, add it to the applied element
    applyFbActiveClass() {
        if (this.FbClass.length > 0) {
            switch (this.FbClass) {
                case "primary": //using global primary
                    this.renderer.addClass(this.elementRef.nativeElement, "m3-global-primary-bg");
                    break;
                case "accent": //using accent primary
                    this.renderer.addClass(this.elementRef.nativeElement, "m3-global-accent-bg");
                    break;
                case "warn": //using warn primary
                    this.renderer.addClass(this.elementRef.nativeElement, "m3-global-warn-bg");
                    break;
                default: //using custom class
                    this.renderer.addClass(this.elementRef.nativeElement, this.FbClass);
                    break;
            }
        }
    }
    //If a class is specified remove it from the applied element
    removeFbActiveClass() {
        if (this.FbClass.length > 0) {
            switch (this.FbClass) {
                case "primary": //using global primary
                    this.renderer.removeClass(this.elementRef.nativeElement, "m3-global-primary-bg");
                    break;
                case "accent": //using accent primary
                    this.renderer.removeClass(this.elementRef.nativeElement, "m3-global-accent-bg");
                    break;
                case "warn": //using warn primary
                    this.renderer.removeClass(this.elementRef.nativeElement, "m3-global-warn-bg");
                    break;
                default: //using custom class
                    this.renderer.removeClass(this.elementRef.nativeElement, this.FbClass);
                    break;
            }
        }
    }
}
CresDigitalFeedbackDirective.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: CresDigitalFeedbackDirective, deps: [{ token: i0.Renderer2 }, { token: i0.ElementRef }, { token: i0.ChangeDetectorRef }], target: i0.ɵɵFactoryTarget.Directive });
CresDigitalFeedbackDirective.ɵdir = i0.ɵɵngDeclareDirective({ minVersion: "12.0.0", version: "13.0.2", type: CresDigitalFeedbackDirective, selector: "[CresDigitalFeedback]", inputs: { FbSignal: "FbSignal", FbClass: "FbClass" }, ngImport: i0 });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: CresDigitalFeedbackDirective, decorators: [{
            type: Directive,
            args: [{
                    selector: '[CresDigitalFeedback]'
                }]
        }], ctorParameters: function () { return [{ type: i0.Renderer2 }, { type: i0.ElementRef }, { type: i0.ChangeDetectorRef }]; }, propDecorators: { FbSignal: [{
                type: Input
            }], FbClass: [{
                type: Input
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3Jlcy1kaWdpdGFsLWZlZWRiYWNrLmRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3Byb2plY3RzL2NyZXMtY29tLXdyYXBwZXIvc3JjL2xpYi9jcmVzLWRpZ2l0YWwtZmVlZGJhY2suZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFDTCxTQUFTLEVBR1QsS0FBSyxFQUdFLE1BQU0sZUFBZSxDQUFDOztBQU8vQixNQUFNLE9BQU8sNEJBQTRCO0lBUXZDLFlBQW9CLFFBQW1CLEVBQzdCLFVBQXNCLEVBQ3RCLGVBQWlDO1FBRnZCLGFBQVEsR0FBUixRQUFRLENBQVc7UUFDN0IsZUFBVSxHQUFWLFVBQVUsQ0FBWTtRQUN0QixvQkFBZSxHQUFmLGVBQWUsQ0FBa0I7UUFSbEMsYUFBUSxHQUFVLEVBQUUsQ0FBQztRQUNyQixZQUFPLEdBQVUsRUFBRSxDQUFDO1FBRTdCLDZDQUE2QztRQUM3QyxrQkFBYSxHQUFVLEVBQUUsQ0FBQztJQUlxQixDQUFDO0lBR2hELFFBQVE7UUFDTixJQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sRUFBRSxDQUFDO0lBQ2hDLENBQUM7SUFFRCxvREFBb0Q7SUFDcEQsZUFBZTtRQUNiLElBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQzNCLElBQUksQ0FBQyxhQUFhLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxHQUFHLEVBQUMsSUFBSSxDQUFDLFFBQVEsRUFBQyxDQUFDLEtBQWEsRUFBQyxFQUFFO2dCQUM5RSxJQUFHLEtBQUssRUFBRTtvQkFDUixJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztpQkFDM0I7cUJBQU07b0JBQ0wsSUFBSSxDQUFDLG1CQUFtQixFQUFFLENBQUM7aUJBQzVCO2dCQUNELElBQUksQ0FBQyxlQUFlLENBQUMsYUFBYSxFQUFFLENBQUM7WUFDdkMsQ0FBQyxDQUFDLENBQUE7U0FDSDtJQUNILENBQUM7SUFDRCxzREFBc0Q7SUFDdEQsV0FBVztRQUNULElBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLEdBQUcsQ0FBQztZQUNoQyxRQUFRLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxFQUFDLElBQUksQ0FBQyxRQUFRLEVBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFBO0lBQ2pFLENBQUM7SUFFRCx3REFBd0Q7SUFDeEQsa0JBQWtCO1FBQ2hCLElBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUMxQjtZQUNBLFFBQVEsSUFBSSxDQUFDLE9BQU8sRUFBRTtnQkFDcEIsS0FBSyxTQUFTLEVBQUUsc0JBQXNCO29CQUNwQyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsRUFBQyxzQkFBc0IsQ0FBQyxDQUFBO29CQUM1RSxNQUFNO2dCQUNSLEtBQUssUUFBUSxFQUFFLHNCQUFzQjtvQkFDbkMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLEVBQUMscUJBQXFCLENBQUMsQ0FBQTtvQkFDM0UsTUFBTTtnQkFDUixLQUFLLE1BQU0sRUFBRSxvQkFBb0I7b0JBQy9CLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxFQUFDLG1CQUFtQixDQUFDLENBQUE7b0JBQ3pFLE1BQU07Z0JBQ1IsU0FBWSxvQkFBb0I7b0JBQzlCLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQTtvQkFDbkUsTUFBTTthQUNQO1NBQ0Y7SUFDSCxDQUFDO0lBRUQsNERBQTREO0lBQzVELG1CQUFtQjtRQUNqQixJQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxHQUFHLENBQUMsRUFDMUI7WUFDQSxRQUFRLElBQUksQ0FBQyxPQUFPLEVBQUU7Z0JBQ3BCLEtBQUssU0FBUyxFQUFFLHNCQUFzQjtvQkFDcEMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLEVBQUMsc0JBQXNCLENBQUMsQ0FBQTtvQkFDL0UsTUFBTTtnQkFDUixLQUFLLFFBQVEsRUFBRSxzQkFBc0I7b0JBQ25DLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxFQUFDLHFCQUFxQixDQUFDLENBQUE7b0JBQzlFLE1BQU07Z0JBQ1IsS0FBSyxNQUFNLEVBQUUsb0JBQW9CO29CQUMvQixJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsRUFBQyxtQkFBbUIsQ0FBQyxDQUFBO29CQUM1RSxNQUFNO2dCQUNSLFNBQVksb0JBQW9CO29CQUM5QixJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUE7b0JBQ3RFLE1BQU07YUFDUDtTQUNGO0lBQ0gsQ0FBQzs7eUhBNUVVLDRCQUE0Qjs2R0FBNUIsNEJBQTRCOzJGQUE1Qiw0QkFBNEI7a0JBSHhDLFNBQVM7bUJBQUM7b0JBQ1QsUUFBUSxFQUFFLHVCQUF1QjtpQkFDbEM7eUpBR1UsUUFBUTtzQkFBaEIsS0FBSztnQkFDRyxPQUFPO3NCQUFmLEtBQUsiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBBZnRlclZpZXdJbml0LFxuICBEaXJlY3RpdmUsXG4gIEVsZW1lbnRSZWYsXG4gIFJlbmRlcmVyMixcbiAgSW5wdXQsXG4gIE9uRGVzdHJveSwgXG4gIENoYW5nZURldGVjdG9yUmVmLFxuICBPbkluaXR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5kZWNsYXJlIHZhciBDckNvbUxpYjogYW55O1xuXG5ARGlyZWN0aXZlKHtcbiAgc2VsZWN0b3I6ICdbQ3Jlc0RpZ2l0YWxGZWVkYmFja10nXG59KVxuZXhwb3J0IGNsYXNzIENyZXNEaWdpdGFsRmVlZGJhY2tEaXJlY3RpdmUgaW1wbGVtZW50cyBPbkluaXQsIEFmdGVyVmlld0luaXQsIE9uRGVzdHJveXtcblxuICBASW5wdXQoKSBGYlNpZ25hbDpzdHJpbmcgPSBcIlwiO1xuICBASW5wdXQoKSBGYkNsYXNzOnN0cmluZyA9IFwiXCI7XG5cbiAgLy9zdG9yZXMgdGhlIHN1YnNjcmlwdGlvbiBJRCBmb3IgdGhlIEZCIHN0YXRlXG4gIENyZXNTdWJIYW5kZWw6c3RyaW5nID0gXCJcIjtcblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIHJlbmRlcmVyOiBSZW5kZXJlcjIsXG4gICAgcHJpdmF0ZSBlbGVtZW50UmVmOiBFbGVtZW50UmVmLFxuICAgIHByaXZhdGUgY2hhbmdlRGV0ZWN0aW9uOkNoYW5nZURldGVjdG9yUmVmKSB7IH1cblxuXG4gIG5nT25Jbml0KCk6IHZvaWQge1xuICAgIHRoaXMuY2hhbmdlRGV0ZWN0aW9uLmRldGFjaCgpO1xuICB9XG5cbiAgLy9JZiBhIEZlZWRiYWNrIHNpZ25hbCBpcyBzcGVjaWZpZWQsIHN1YnNjcmliZSB0byBpdFxuICBuZ0FmdGVyVmlld0luaXQoKSB7XG4gICAgaWYodGhpcy5GYlNpZ25hbC5sZW5ndGggPiAwKSB7XG4gICAgICB0aGlzLkNyZXNTdWJIYW5kZWwgPSBDckNvbUxpYi5zdWJzY3JpYmVTdGF0ZSgnYicsdGhpcy5GYlNpZ25hbCwoc3RhdGU6Ym9vbGVhbik9PiB7XG4gICAgICAgIGlmKHN0YXRlKSB7XG4gICAgICAgICAgdGhpcy5hcHBseUZiQWN0aXZlQ2xhc3MoKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICB0aGlzLnJlbW92ZUZiQWN0aXZlQ2xhc3MoKTtcbiAgICAgICAgfVxuICAgICAgICB0aGlzLmNoYW5nZURldGVjdGlvbi5kZXRlY3RDaGFuZ2VzKCk7XG4gICAgICB9KVxuICAgIH1cbiAgfVxuICAvL0NsZWFuIHVwIEZCIHN1YiBpZiBpdCBleGlzdHMsIHByZXZlbnQgbWVtb3J5IGxlZWthZ2VcbiAgbmdPbkRlc3Ryb3koKSB7XG4gICAgaWYodGhpcy5DcmVzU3ViSGFuZGVsLmxlbmd0aCA+IDApXG4gICAgQ3JDb21MaWIudW5zdWJzY3JpYmVTdGF0ZSgnYicsdGhpcy5GYlNpZ25hbCx0aGlzLkNyZXNTdWJIYW5kZWwpXG4gIH1cblxuICAvL0lmIGEgY2xhc3MgaXMgc3BlY2lmaWVkLCBhZGQgaXQgdG8gdGhlIGFwcGxpZWQgZWxlbWVudFxuICBhcHBseUZiQWN0aXZlQ2xhc3MoKSB7XG4gICAgaWYodGhpcy5GYkNsYXNzLmxlbmd0aCA+IDApXG4gICAge1xuICAgIHN3aXRjaCAodGhpcy5GYkNsYXNzKSB7XG4gICAgICBjYXNlIFwicHJpbWFyeVwiOiAvL3VzaW5nIGdsb2JhbCBwcmltYXJ5XG4gICAgICAgIHRoaXMucmVuZGVyZXIuYWRkQ2xhc3ModGhpcy5lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnQsXCJtMy1nbG9iYWwtcHJpbWFyeS1iZ1wiKVxuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgXCJhY2NlbnRcIjogLy91c2luZyBhY2NlbnQgcHJpbWFyeVxuICAgICAgICB0aGlzLnJlbmRlcmVyLmFkZENsYXNzKHRoaXMuZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50LFwibTMtZ2xvYmFsLWFjY2VudC1iZ1wiKVxuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgXCJ3YXJuXCI6IC8vdXNpbmcgd2FybiBwcmltYXJ5XG4gICAgICAgIHRoaXMucmVuZGVyZXIuYWRkQ2xhc3ModGhpcy5lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnQsXCJtMy1nbG9iYWwtd2Fybi1iZ1wiKVxuICAgICAgICBicmVhaztcbiAgICAgIGRlZmF1bHQ6ICAgIC8vdXNpbmcgY3VzdG9tIGNsYXNzXG4gICAgICAgIHRoaXMucmVuZGVyZXIuYWRkQ2xhc3ModGhpcy5lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnQsIHRoaXMuRmJDbGFzcylcbiAgICAgICAgYnJlYWs7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgLy9JZiBhIGNsYXNzIGlzIHNwZWNpZmllZCByZW1vdmUgaXQgZnJvbSB0aGUgYXBwbGllZCBlbGVtZW50XG4gIHJlbW92ZUZiQWN0aXZlQ2xhc3MoKSB7XG4gICAgaWYodGhpcy5GYkNsYXNzLmxlbmd0aCA+IDApXG4gICAge1xuICAgIHN3aXRjaCAodGhpcy5GYkNsYXNzKSB7XG4gICAgICBjYXNlIFwicHJpbWFyeVwiOiAvL3VzaW5nIGdsb2JhbCBwcmltYXJ5XG4gICAgICAgIHRoaXMucmVuZGVyZXIucmVtb3ZlQ2xhc3ModGhpcy5lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnQsXCJtMy1nbG9iYWwtcHJpbWFyeS1iZ1wiKVxuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgXCJhY2NlbnRcIjogLy91c2luZyBhY2NlbnQgcHJpbWFyeVxuICAgICAgICB0aGlzLnJlbmRlcmVyLnJlbW92ZUNsYXNzKHRoaXMuZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50LFwibTMtZ2xvYmFsLWFjY2VudC1iZ1wiKVxuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgXCJ3YXJuXCI6IC8vdXNpbmcgd2FybiBwcmltYXJ5XG4gICAgICAgIHRoaXMucmVuZGVyZXIucmVtb3ZlQ2xhc3ModGhpcy5lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnQsXCJtMy1nbG9iYWwtd2Fybi1iZ1wiKVxuICAgICAgICBicmVhaztcbiAgICAgIGRlZmF1bHQ6ICAgIC8vdXNpbmcgY3VzdG9tIGNsYXNzXG4gICAgICAgIHRoaXMucmVuZGVyZXIucmVtb3ZlQ2xhc3ModGhpcy5lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnQsIHRoaXMuRmJDbGFzcylcbiAgICAgICAgYnJlYWs7XG4gICAgICB9XG4gICAgfVxuICB9XG59XG4iXX0=