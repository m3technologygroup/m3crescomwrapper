import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
export class SendString {
    constructor() { }
    static Value(signal, value) {
        CrComLib.publishEvent('s', signal, value);
    }
}
SendString.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: SendString, deps: [], target: i0.ɵɵFactoryTarget.Injectable });
SendString.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: SendString, providedIn: 'root' });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: SendString, decorators: [{
            type: Injectable,
            args: [{
                    providedIn: 'root'
                }]
        }], ctorParameters: function () { return []; } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VuZC1zdHJpbmcuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi9wcm9qZWN0cy9jcmVzLWNvbS13cmFwcGVyL3NyYy9saWIvc2VuZC1zdHJpbmcudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQzs7QUFPM0MsTUFBTSxPQUFPLFVBQVU7SUFFckIsZ0JBQWdCLENBQUM7SUFFakIsTUFBTSxDQUFDLEtBQUssQ0FBQyxNQUFhLEVBQUMsS0FBWTtRQUNyQyxRQUFRLENBQUMsWUFBWSxDQUFDLEdBQUcsRUFBQyxNQUFNLEVBQUMsS0FBSyxDQUFDLENBQUE7SUFDekMsQ0FBQzs7dUdBTlUsVUFBVTsyR0FBVixVQUFVLGNBRlQsTUFBTTsyRkFFUCxVQUFVO2tCQUh0QixVQUFVO21CQUFDO29CQUNWLFVBQVUsRUFBRSxNQUFNO2lCQUNuQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbmRlY2xhcmUgdmFyIENyQ29tTGliOiBhbnk7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBTZW5kU3RyaW5nIHtcclxuXHJcbiAgY29uc3RydWN0b3IoKSB7IH1cclxuXHJcbiAgc3RhdGljIFZhbHVlKHNpZ25hbDpzdHJpbmcsdmFsdWU6c3RyaW5nKTp2b2lkIHtcclxuICAgIENyQ29tTGliLnB1Ymxpc2hFdmVudCgncycsc2lnbmFsLHZhbHVlKVxyXG4gIH1cclxufVxyXG4iXX0=