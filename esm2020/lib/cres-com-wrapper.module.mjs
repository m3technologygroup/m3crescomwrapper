import { NgModule } from '@angular/core';
import { CresDigitalSendDirective } from './cres-digital-send.directive';
import { CresDigitalFeedbackDirective } from "./cres-digital-feedback.directive";
import * as i0 from "@angular/core";
export class CresComWrapperModule {
}
CresComWrapperModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: CresComWrapperModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
CresComWrapperModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: CresComWrapperModule, declarations: [CresDigitalSendDirective,
        CresDigitalFeedbackDirective], exports: [CresDigitalSendDirective,
        CresDigitalFeedbackDirective] });
CresComWrapperModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: CresComWrapperModule, imports: [[]] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: CresComWrapperModule, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [
                        CresDigitalSendDirective,
                        CresDigitalFeedbackDirective
                    ],
                    imports: [],
                    exports: [
                        CresDigitalSendDirective,
                        CresDigitalFeedbackDirective
                    ]
                }]
        }] });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3Jlcy1jb20td3JhcHBlci5tb2R1bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi9wcm9qZWN0cy9jcmVzLWNvbS13cmFwcGVyL3NyYy9saWIvY3Jlcy1jb20td3JhcHBlci5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN6QyxPQUFPLEVBQUMsd0JBQXdCLEVBQUMsTUFBTSwrQkFBK0IsQ0FBQTtBQUN0RSxPQUFPLEVBQUUsNEJBQTRCLEVBQUUsTUFBTSxtQ0FBbUMsQ0FBQzs7QUFlakYsTUFBTSxPQUFPLG9CQUFvQjs7aUhBQXBCLG9CQUFvQjtrSEFBcEIsb0JBQW9CLGlCQVY3Qix3QkFBd0I7UUFDeEIsNEJBQTRCLGFBSzVCLHdCQUF3QjtRQUN4Qiw0QkFBNEI7a0hBR25CLG9CQUFvQixZQVB0QixFQUNSOzJGQU1VLG9CQUFvQjtrQkFaaEMsUUFBUTttQkFBQztvQkFDUixZQUFZLEVBQUU7d0JBQ1osd0JBQXdCO3dCQUN4Qiw0QkFBNEI7cUJBQzdCO29CQUNELE9BQU8sRUFBRSxFQUNSO29CQUNELE9BQU8sRUFBRTt3QkFDUCx3QkFBd0I7d0JBQ3hCLDRCQUE0QjtxQkFDN0I7aUJBQ0YiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtDcmVzRGlnaXRhbFNlbmREaXJlY3RpdmV9IGZyb20gJy4vY3Jlcy1kaWdpdGFsLXNlbmQuZGlyZWN0aXZlJ1xuaW1wb3J0IHsgQ3Jlc0RpZ2l0YWxGZWVkYmFja0RpcmVjdGl2ZSB9IGZyb20gXCIuL2NyZXMtZGlnaXRhbC1mZWVkYmFjay5kaXJlY3RpdmVcIjtcblxuXG5ATmdNb2R1bGUoe1xuICBkZWNsYXJhdGlvbnM6IFtcbiAgICBDcmVzRGlnaXRhbFNlbmREaXJlY3RpdmUsXG4gICAgQ3Jlc0RpZ2l0YWxGZWVkYmFja0RpcmVjdGl2ZVxuICBdLFxuICBpbXBvcnRzOiBbXG4gIF0sXG4gIGV4cG9ydHM6IFtcbiAgICBDcmVzRGlnaXRhbFNlbmREaXJlY3RpdmUsXG4gICAgQ3Jlc0RpZ2l0YWxGZWVkYmFja0RpcmVjdGl2ZVxuICBdXG59KVxuZXhwb3J0IGNsYXNzIENyZXNDb21XcmFwcGVyTW9kdWxlIHsgfVxuIl19