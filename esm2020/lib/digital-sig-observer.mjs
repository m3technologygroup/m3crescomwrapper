import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import * as i0 from "@angular/core";
export class DigitalSigObserver {
    constructor() { }
    static Observe(signal) {
        return new Observable((observer) => {
            let CresSubHandel = CrComLib.subscribeState('b', signal, (val) => {
                observer.next(val);
            });
            return () => {
                CrComLib.unsubscribeState('b', signal, CresSubHandel);
            };
        });
    }
}
DigitalSigObserver.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DigitalSigObserver, deps: [], target: i0.ɵɵFactoryTarget.Injectable });
DigitalSigObserver.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DigitalSigObserver, providedIn: 'root' });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DigitalSigObserver, decorators: [{
            type: Injectable,
            args: [{
                    providedIn: 'root'
                }]
        }], ctorParameters: function () { return []; } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGlnaXRhbC1zaWctb2JzZXJ2ZXIuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi9wcm9qZWN0cy9jcmVzLWNvbS13cmFwcGVyL3NyYy9saWIvZGlnaXRhbC1zaWctb2JzZXJ2ZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sTUFBTSxDQUFDOztBQU9sQyxNQUFNLE9BQU8sa0JBQWtCO0lBRTdCLGdCQUFnQixDQUFDO0lBRWpCLE1BQU0sQ0FBQyxPQUFPLENBQUMsTUFBYTtRQUMxQixPQUFPLElBQUksVUFBVSxDQUFDLENBQUMsUUFBUSxFQUFFLEVBQUU7WUFDakMsSUFBSSxhQUFhLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxHQUFHLEVBQUMsTUFBTSxFQUFFLENBQUMsR0FBVyxFQUFFLEVBQUU7Z0JBQ3RFLFFBQVEsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDckIsQ0FBQyxDQUFDLENBQUE7WUFFRixPQUFPLEdBQUcsRUFBRTtnQkFDVixRQUFRLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxFQUFDLE1BQU0sRUFBQyxhQUFhLENBQUMsQ0FBQTtZQUNyRCxDQUFDLENBQUE7UUFDSCxDQUFDLENBQUMsQ0FBQTtJQUNKLENBQUM7OytHQWRVLGtCQUFrQjttSEFBbEIsa0JBQWtCLGNBRmpCLE1BQU07MkZBRVAsa0JBQWtCO2tCQUg5QixVQUFVO21CQUFDO29CQUNWLFVBQVUsRUFBRSxNQUFNO2lCQUNuQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcblxuZGVjbGFyZSB2YXIgQ3JDb21MaWI6IGFueTtcblxuQEluamVjdGFibGUoe1xuICBwcm92aWRlZEluOiAncm9vdCdcbn0pXG5leHBvcnQgY2xhc3MgRGlnaXRhbFNpZ09ic2VydmVyIHtcblxuICBjb25zdHJ1Y3RvcigpIHsgfVxuXG4gIHN0YXRpYyBPYnNlcnZlKHNpZ25hbDpzdHJpbmcpOk9ic2VydmFibGU8Ym9vbGVhbj4ge1xuICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZSgob2JzZXJ2ZXIpID0+IHtcbiAgICAgIGxldCBDcmVzU3ViSGFuZGVsID0gQ3JDb21MaWIuc3Vic2NyaWJlU3RhdGUoJ2InLHNpZ25hbCwgKHZhbDpib29sZWFuKSA9PiB7XG4gICAgICAgIG9ic2VydmVyLm5leHQodmFsKTtcbiAgICAgIH0pXG4gICAgICBcbiAgICAgIHJldHVybiAoKSA9PiB7XG4gICAgICAgIENyQ29tTGliLnVuc3Vic2NyaWJlU3RhdGUoJ2InLHNpZ25hbCxDcmVzU3ViSGFuZGVsKVxuICAgICAgfVxuICAgIH0pIFxuICB9XG59XG4iXX0=