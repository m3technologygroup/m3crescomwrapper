import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
export class EmulatorLoader {
    constructor() {
        this.ch5Emulator = CrComLib.Ch5Emulator.getInstance();
    }
    // init emulator
    initEmulator(emulator) {
        CrComLib.Ch5Emulator.clear();
        this.ch5Emulator = CrComLib.Ch5Emulator.getInstance();
        this.ch5Emulator.loadScenario(emulator);
        this.ch5Emulator.run();
    }
}
EmulatorLoader.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: EmulatorLoader, deps: [], target: i0.ɵɵFactoryTarget.Injectable });
EmulatorLoader.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: EmulatorLoader, providedIn: 'root' });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: EmulatorLoader, decorators: [{
            type: Injectable,
            args: [{
                    providedIn: 'root'
                }]
        }], ctorParameters: function () { return []; } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZW11bGF0b3ItbG9hZGVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vcHJvamVjdHMvY3Jlcy1jb20td3JhcHBlci9zcmMvbGliL2VtdWxhdG9yLWxvYWRlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDOztBQU8zQyxNQUFNLE9BQU8sY0FBYztJQUV6QjtRQURBLGdCQUFXLEdBQWUsUUFBUSxDQUFDLFdBQVcsQ0FBQyxXQUFXLEVBQUUsQ0FBQztJQUM3QyxDQUFDO0lBRWpCLGdCQUFnQjtJQUNULFlBQVksQ0FBQyxRQUFZO1FBQzlCLFFBQVEsQ0FBQyxXQUFXLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDN0IsSUFBSSxDQUFDLFdBQVcsR0FBRyxRQUFRLENBQUMsV0FBVyxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ3RELElBQUksQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ3hDLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxFQUFFLENBQUM7SUFDekIsQ0FBQzs7MkdBVlUsY0FBYzsrR0FBZCxjQUFjLGNBRmIsTUFBTTsyRkFFUCxjQUFjO2tCQUgxQixVQUFVO21CQUFDO29CQUNWLFVBQVUsRUFBRSxNQUFNO2lCQUNuQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IENoNUVtdWxhdG9yIH0gZnJvbSBcIkBjcmVzdHJvbi9jaDUtY3Jjb21saWIvYnVpbGRfYnVuZGxlcy91bWQvQHR5cGVzXCJcbmRlY2xhcmUgdmFyIENyQ29tTGliOiBhbnk7XG5cbkBJbmplY3RhYmxlKHtcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuZXhwb3J0IGNsYXNzIEVtdWxhdG9yTG9hZGVyIHtcbiAgY2g1RW11bGF0b3I6Q2g1RW11bGF0b3IgPSBDckNvbUxpYi5DaDVFbXVsYXRvci5nZXRJbnN0YW5jZSgpO1xuICBjb25zdHJ1Y3RvcigpIHsgfVxuIFxuICAvLyBpbml0IGVtdWxhdG9yXG4gIHB1YmxpYyBpbml0RW11bGF0b3IoZW11bGF0b3I6YW55KSB7XG4gICAgQ3JDb21MaWIuQ2g1RW11bGF0b3IuY2xlYXIoKTtcbiAgICB0aGlzLmNoNUVtdWxhdG9yID0gQ3JDb21MaWIuQ2g1RW11bGF0b3IuZ2V0SW5zdGFuY2UoKTtcbiAgICB0aGlzLmNoNUVtdWxhdG9yLmxvYWRTY2VuYXJpbyhlbXVsYXRvcik7XG4gICAgdGhpcy5jaDVFbXVsYXRvci5ydW4oKTtcbiAgfVxufVxuIl19