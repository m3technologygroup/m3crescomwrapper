import { Directive, HostListener, Input } from '@angular/core';
import * as i0 from "@angular/core";
export class CresDigitalSendDirective {
    constructor() {
        //handel for the interval used for press and hold
        this.interval = null;
        this.Signal = "";
        this.PressHold = "false";
    }
    //handel Toutch inputs
    onTouchStart(ev) {
        ev.preventDefault();
        if (this.PressHold === "true") {
            this.sendPress();
        }
        else {
            this.sendPulse();
        }
    }
    onTouchEnd(ev) {
        ev.preventDefault();
        if (this.PressHold === "true") {
            this.sendRelease();
        }
    }
    //Handel Mouse inputs for Xpanel
    onMouseDown() {
        if (this.PressHold === "true") {
            this.sendPress();
        }
        else {
            this.sendPulse();
        }
    }
    onMouseUp() {
        if (this.PressHold === "true") {
            this.sendRelease();
        }
    }
    //send an instantanious pulse to the CS
    sendPulse() {
        if (this.Signal.length > 0) {
            CrComLib.publishEvent('b', this.Signal, true);
            CrComLib.publishEvent('b', this.Signal, false);
        }
    }
    //send repeat digital event for press and hold.
    //250ms repeat time required by crestron
    sendPress() {
        CrComLib.publishEvent('o', this.Signal, { repeatdigital: true });
        this.interval = setInterval(() => {
            CrComLib.publishEvent('o', this.Signal, { repeatdigital: true });
        }, 250);
    }
    sendRelease() {
        if (this.interval !== null) {
            clearInterval(this.interval);
        }
        CrComLib.publishEvent('o', this.Signal, { repeatdigital: false });
    }
}
CresDigitalSendDirective.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: CresDigitalSendDirective, deps: [], target: i0.ɵɵFactoryTarget.Directive });
CresDigitalSendDirective.ɵdir = i0.ɵɵngDeclareDirective({ minVersion: "12.0.0", version: "13.0.2", type: CresDigitalSendDirective, selector: "[CresDigitalSend]", inputs: { Signal: "Signal", PressHold: "PressHold" }, host: { listeners: { "touchstart": "onTouchStart($event)", "touchend": "onTouchEnd($event)", "mousedown": "onMouseDown()", "mouseup": "onMouseUp()" } }, ngImport: i0 });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: CresDigitalSendDirective, decorators: [{
            type: Directive,
            args: [{
                    selector: '[CresDigitalSend]'
                }]
        }], ctorParameters: function () { return []; }, propDecorators: { Signal: [{
                type: Input
            }], PressHold: [{
                type: Input
            }], onTouchStart: [{
                type: HostListener,
                args: ['touchstart', ['$event']]
            }], onTouchEnd: [{
                type: HostListener,
                args: ['touchend', ['$event']]
            }], onMouseDown: [{
                type: HostListener,
                args: ['mousedown']
            }], onMouseUp: [{
                type: HostListener,
                args: ['mouseup']
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3Jlcy1kaWdpdGFsLXNlbmQuZGlyZWN0aXZlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vcHJvamVjdHMvY3Jlcy1jb20td3JhcHBlci9zcmMvbGliL2NyZXMtZGlnaXRhbC1zZW5kLmRpcmVjdGl2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUMsU0FBUyxFQUFDLFlBQVksRUFBQyxLQUFLLEVBQUMsTUFBTSxlQUFlLENBQUM7O0FBTzNELE1BQU0sT0FBTyx3QkFBd0I7SUFFbkM7UUFFQSxpREFBaUQ7UUFDakQsYUFBUSxHQUFPLElBQUksQ0FBQztRQUVYLFdBQU0sR0FBVSxFQUFFLENBQUM7UUFDbkIsY0FBUyxHQUFxQixPQUFPLENBQUM7SUFOL0IsQ0FBQztJQVFqQixzQkFBc0I7SUFDaUIsWUFBWSxDQUFDLEVBQU07UUFDeEQsRUFBRSxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBQ3BCLElBQUcsSUFBSSxDQUFDLFNBQVMsS0FBSyxNQUFNLEVBQUM7WUFDM0IsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFBO1NBQ2pCO2FBQU07WUFDTCxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUE7U0FDakI7SUFDSCxDQUFDO0lBRW9DLFVBQVUsQ0FBQyxFQUFNO1FBQ3BELEVBQUUsQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUNwQixJQUFHLElBQUksQ0FBQyxTQUFTLEtBQUssTUFBTSxFQUFFO1lBQzVCLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQTtTQUNuQjtJQUNILENBQUM7SUFFRCxnQ0FBZ0M7SUFDTCxXQUFXO1FBQ3BDLElBQUcsSUFBSSxDQUFDLFNBQVMsS0FBSyxNQUFNLEVBQUM7WUFDM0IsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFBO1NBQ2pCO2FBQU07WUFDTCxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUE7U0FDakI7SUFDSCxDQUFDO0lBQ3dCLFNBQVM7UUFDaEMsSUFBRyxJQUFJLENBQUMsU0FBUyxLQUFLLE1BQU0sRUFBRTtZQUM1QixJQUFJLENBQUMsV0FBVyxFQUFFLENBQUE7U0FDbkI7SUFDSCxDQUFDO0lBRUQsdUNBQXVDO0lBQ3ZDLFNBQVM7UUFDUCxJQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUN6QixRQUFRLENBQUMsWUFBWSxDQUFDLEdBQUcsRUFBQyxJQUFJLENBQUMsTUFBTSxFQUFDLElBQUksQ0FBQyxDQUFDO1lBQzVDLFFBQVEsQ0FBQyxZQUFZLENBQUMsR0FBRyxFQUFDLElBQUksQ0FBQyxNQUFNLEVBQUMsS0FBSyxDQUFDLENBQUM7U0FDOUM7SUFDSCxDQUFDO0lBRUQsK0NBQStDO0lBQy9DLHdDQUF3QztJQUN4QyxTQUFTO1FBQ1AsUUFBUSxDQUFDLFlBQVksQ0FBQyxHQUFHLEVBQUMsSUFBSSxDQUFDLE1BQU0sRUFBQyxFQUFDLGFBQWEsRUFBRyxJQUFJLEVBQUMsQ0FBQyxDQUFBO1FBQzdELElBQUksQ0FBQyxRQUFRLEdBQUcsV0FBVyxDQUFDLEdBQUcsRUFBRTtZQUMvQixRQUFRLENBQUMsWUFBWSxDQUFDLEdBQUcsRUFBQyxJQUFJLENBQUMsTUFBTSxFQUFDLEVBQUMsYUFBYSxFQUFHLElBQUksRUFBQyxDQUFDLENBQUE7UUFDL0QsQ0FBQyxFQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ1QsQ0FBQztJQUNELFdBQVc7UUFDVCxJQUFHLElBQUksQ0FBQyxRQUFRLEtBQUssSUFBSSxFQUFFO1lBQ3pCLGFBQWEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7U0FDOUI7UUFDRCxRQUFRLENBQUMsWUFBWSxDQUFDLEdBQUcsRUFBQyxJQUFJLENBQUMsTUFBTSxFQUFDLEVBQUMsYUFBYSxFQUFHLEtBQUssRUFBQyxDQUFDLENBQUE7SUFDaEUsQ0FBQzs7cUhBOURVLHdCQUF3Qjt5R0FBeEIsd0JBQXdCOzJGQUF4Qix3QkFBd0I7a0JBSHBDLFNBQVM7bUJBQUM7b0JBQ1QsUUFBUSxFQUFFLG1CQUFtQjtpQkFDOUI7MEVBUVUsTUFBTTtzQkFBZCxLQUFLO2dCQUNHLFNBQVM7c0JBQWpCLEtBQUs7Z0JBR2lDLFlBQVk7c0JBQWxELFlBQVk7dUJBQUMsWUFBWSxFQUFDLENBQUMsUUFBUSxDQUFDO2dCQVNBLFVBQVU7c0JBQTlDLFlBQVk7dUJBQUMsVUFBVSxFQUFDLENBQUMsUUFBUSxDQUFDO2dCQVFSLFdBQVc7c0JBQXJDLFlBQVk7dUJBQUMsV0FBVztnQkFPQSxTQUFTO3NCQUFqQyxZQUFZO3VCQUFDLFNBQVMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0RpcmVjdGl2ZSxIb3N0TGlzdGVuZXIsSW5wdXR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5kZWNsYXJlIHZhciBDckNvbUxpYjogYW55O1xuXG5ARGlyZWN0aXZlKHtcbiAgc2VsZWN0b3I6ICdbQ3Jlc0RpZ2l0YWxTZW5kXSdcbn0pXG5leHBvcnQgY2xhc3MgQ3Jlc0RpZ2l0YWxTZW5kRGlyZWN0aXZlIHtcblxuICBjb25zdHJ1Y3RvcigpIHsgfVxuXG4gIC8vaGFuZGVsIGZvciB0aGUgaW50ZXJ2YWwgdXNlZCBmb3IgcHJlc3MgYW5kIGhvbGRcbiAgaW50ZXJ2YWw6YW55ID0gbnVsbDtcblxuICBASW5wdXQoKSBTaWduYWw6c3RyaW5nID0gXCJcIjtcbiAgQElucHV0KCkgUHJlc3NIb2xkOiBcInRydWVcIiB8IFwiZmFsc2VcIiA9IFwiZmFsc2VcIjtcblxuICAvL2hhbmRlbCBUb3V0Y2ggaW5wdXRzXG4gIEBIb3N0TGlzdGVuZXIoJ3RvdWNoc3RhcnQnLFsnJGV2ZW50J10pIG9uVG91Y2hTdGFydChldjphbnkpIHtcbiAgICBldi5wcmV2ZW50RGVmYXVsdCgpO1xuICAgIGlmKHRoaXMuUHJlc3NIb2xkID09PSBcInRydWVcIil7XG4gICAgICB0aGlzLnNlbmRQcmVzcygpXG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMuc2VuZFB1bHNlKClcbiAgICB9XG4gIH1cblxuICBASG9zdExpc3RlbmVyKCd0b3VjaGVuZCcsWyckZXZlbnQnXSkgb25Ub3VjaEVuZChldjphbnkpIHtcbiAgICBldi5wcmV2ZW50RGVmYXVsdCgpO1xuICAgIGlmKHRoaXMuUHJlc3NIb2xkID09PSBcInRydWVcIikge1xuICAgICAgdGhpcy5zZW5kUmVsZWFzZSgpXG4gICAgfVxuICB9XG5cbiAgLy9IYW5kZWwgTW91c2UgaW5wdXRzIGZvciBYcGFuZWxcbiAgQEhvc3RMaXN0ZW5lcignbW91c2Vkb3duJykgb25Nb3VzZURvd24oKSB7XG4gICAgaWYodGhpcy5QcmVzc0hvbGQgPT09IFwidHJ1ZVwiKXtcbiAgICAgIHRoaXMuc2VuZFByZXNzKClcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5zZW5kUHVsc2UoKVxuICAgIH1cbiAgfVxuICBASG9zdExpc3RlbmVyKCdtb3VzZXVwJykgb25Nb3VzZVVwKCkge1xuICAgIGlmKHRoaXMuUHJlc3NIb2xkID09PSBcInRydWVcIikge1xuICAgICAgdGhpcy5zZW5kUmVsZWFzZSgpXG4gICAgfVxuICB9XG5cbiAgLy9zZW5kIGFuIGluc3RhbnRhbmlvdXMgcHVsc2UgdG8gdGhlIENTXG4gIHNlbmRQdWxzZSgpIHtcbiAgICBpZih0aGlzLlNpZ25hbC5sZW5ndGggPiAwKSB7XG4gICAgICBDckNvbUxpYi5wdWJsaXNoRXZlbnQoJ2InLHRoaXMuU2lnbmFsLHRydWUpO1xuICAgICAgQ3JDb21MaWIucHVibGlzaEV2ZW50KCdiJyx0aGlzLlNpZ25hbCxmYWxzZSk7XG4gICAgfVxuICB9XG5cbiAgLy9zZW5kIHJlcGVhdCBkaWdpdGFsIGV2ZW50IGZvciBwcmVzcyBhbmQgaG9sZC5cbiAgLy8yNTBtcyByZXBlYXQgdGltZSByZXF1aXJlZCBieSBjcmVzdHJvblxuICBzZW5kUHJlc3MoKSB7XG4gICAgQ3JDb21MaWIucHVibGlzaEV2ZW50KCdvJyx0aGlzLlNpZ25hbCx7cmVwZWF0ZGlnaXRhbCA6IHRydWV9KVxuICAgIHRoaXMuaW50ZXJ2YWwgPSBzZXRJbnRlcnZhbCgoKSA9PiB7XG4gICAgICBDckNvbUxpYi5wdWJsaXNoRXZlbnQoJ28nLHRoaXMuU2lnbmFsLHtyZXBlYXRkaWdpdGFsIDogdHJ1ZX0pXG4gICAgfSwyNTApO1xuICB9XG4gIHNlbmRSZWxlYXNlKCkge1xuICAgIGlmKHRoaXMuaW50ZXJ2YWwgIT09IG51bGwpIHtcbiAgICAgIGNsZWFySW50ZXJ2YWwodGhpcy5pbnRlcnZhbCk7XG4gICAgfVxuICAgIENyQ29tTGliLnB1Ymxpc2hFdmVudCgnbycsdGhpcy5TaWduYWwse3JlcGVhdGRpZ2l0YWwgOiBmYWxzZX0pXG4gIH1cblxufVxuIl19